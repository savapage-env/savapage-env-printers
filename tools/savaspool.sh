#!/bin/bash
#
# This file is part of the SavaPage project <https://www.savapage.org>.
# Copyright (c) 2011-2024 Datraverse B.V.
# Author: Rijk Ravestein.
#
# SPDX-FileCopyrightText: 2011-2024 Datraverse B.V. <info@datraverse.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For more information, please contact Datraverse B.V. at this
# address: info@datraverse.com
#

# -------------------------------------------------------------------
# This simple CUPS backend allows you to save a print spool file. 
# Install the backend like this:
#
# sudo cp <this.file> /usr/lib/cups/backend/savaspool
# sudo chmod 500 /usr/lib/cups/backend/savaspool
# sudo chown root:root /usr/lib/cups/backend/savaspool
#
# Device-URI: 
#   savaspool:/path/to/basefilename
#
# Spoolfiles are named
#   savaspool:/path/to/basefilename-[jobid]-[title].prn
#
# URI example: capture spoolfiles of CUPS printer CANON-IR-ADV-C5550
#   savaspool:/tmp/savaspool-canon-ir-adv-c5550

# Metadata of spoolfiles are logged in /tmp/savaspool.log
# -------------------------------------------------------------------

#
# No arguments means show available devices...
#
if [ $# -eq 0 ]; then
    echo "direct savaspool \"Unknown\" \"Print any job to file specified in device-URI\""
    exit 0;
fi

#
# If wrong number of arguments: print usage hint
#
if [ $# -ne 5 -a $# -ne 6 ]; then
    echo "+-----------------------------------------------------------------------+"
    echo "| SavaSpool is a CUPS backend that writes a print spool file to disk at |"
    echo "| the path in Device URI with format: 'savaspool:/path/to/basefilename' |"
    echo "+-----------------------------------------------------------------------+"
    echo "  Usage: savaspool job-id user title copies options [file]"
    echo ""
    exit 1
fi

#
# Collect arguments...
#
readonly JOBID=$1 ;
readonly USER=$2 ;
readonly TITLE_RAW=$3 ;
readonly COPIES=$4 ;
readonly OPTIONS=$5 ;
readonly PID=$$ ;

if [ $# -eq 5 ]; then
    readonly FILE="-" ;
else
    readonly FILE=$6 ;
fi

#
# Sanitize the $TITLE_RAW by removing spaces, colons and (back) slashes.
#
readonly TITLE=`echo ${TITLE_RAW} | tr [:blank:] _ | tr : _ | tr / _ | tr "\134" _`;

#
# Use the base filename from the device URI, to compose a unique spoolfile name
# so this backend can be used multiple times, for various "savaspool" printers.
#
readonly TARGETFILE=${DEVICE_URI#savaspool:}-${JOBID}-${TITLE}.prn

# Log
LOG=/tmp/savaspool.log; export LOG;

echo "---------------------------------" >> ${LOG} ;
echo "Job ID   : ${JOBID}" >> ${LOG} ;
echo "Start    : `date`" >> ${LOG} ;
echo "File in  : ${FILE}" >> ${LOG} ;
echo "File out : ${TARGETFILE}" >> ${LOG} ;
echo "User     : ${USER}" >> ${LOG} ;
echo "Title    : ${TITLE_RAW}" >> ${LOG} ;
echo "Copies   : ${COPIES}" >> ${LOG} ;
echo "Options  : ${OPTIONS}" >> ${LOG} ;

# Save the spoolfile
cat $FILE > $TARGETFILE

echo "---------------------------------" >> ${LOG} ;
echo "Job ID   : ${JOBID}" >> ${LOG} ;
echo "Finish   : `date`" >> ${LOG} ;


# Log you how cupsd logs stuff....
echo "INFO: saved spoolfile ${TARGETFILE} ">&2

# When needed use...
#echo "ERROR: error saving spoolfile ${TARGETFILE} ">&2

exit 0

# end-of-file
