# -----------------------------------------------------------------------------
# This file is part of the SavaPage project <https://www.savapage.org>.
# Copyright (c) 2011-2024 Datraverse B.V.
# Author: Rijk Ravestein.
#
# SPDX-FileCopyrightText: 2011-2024 Datraverse B.V. <info@datraverse.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file; if not, see <https://www.gnu.org/licenses/>
#
# For more information, please contact Datraverse B.V. at this
# address: info@datraverse.com
# -----------------------------------------------------------------------------

#-----------------------------------------------------------------------
#*PCFileName: "EPL5590 Series.PPD"
#*ModelName: "EPSON L5590 Series"
#-----------------------------------------------------------------------

#-----------------------------------------------------------------------
# PPD "Ink" to IPP "print-color-mode"
#-----------------------------------------------------------------------
*Ink print-color-mode
*Ink *MONO monochrome
*Ink COLOR color

#-----------------------------------------------------------------------
# PPD has no media-source defined. As a hack, map PPD PageRegion 
# attribute values A4, A5 and A6 to virtual IPP trays.
#-----------------------------------------------------------------------
*PageRegion media-source
*PageRegion *A4 tray-a4
*PageRegion  A5 tray-a5
*PageRegion  A6 tray-a6

#-----------------------------------------------------------------------
# Map the PPD PageSize A4, A5 and A6 to IPP attribute "media".
#-----------------------------------------------------------------------
*PageSize media
*PageSize *A4 iso_a4_210x297mm
*PageSize  A5 iso_a5_148x210mm
*PageSize  A6 iso_a6_105x148mm

#-----------------------------------------------------------------------
# In "Admin Web App: Proxy Printer - Edit - Media Source" 
# [x] tray-a4 : name "A4", media A4
# [x] tray-a5 : name "A5", media A5
# [x] tray-a6 : name "A6", media A6
#-----------------------------------------------------------------------

# end-of-file
