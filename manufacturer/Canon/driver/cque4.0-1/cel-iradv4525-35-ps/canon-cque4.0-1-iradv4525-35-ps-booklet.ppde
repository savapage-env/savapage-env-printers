# -----------------------------------------------------------------------------
# This file is part of the SavaPage project <https://www.savapage.org>.
# Copyright (c) 2011-2020 Datraverse B.V.
# Author: Rijk Ravestein.
#
# SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file; if not, see <https://www.gnu.org/licenses/>
#
# For more information, please contact Datraverse B.V. at this
# address: info@datraverse.com
# -----------------------------------------------------------------------------

#-----------------------------------------------------------------------
#*PCFileName: "CEIRADV4.PPD"
#*ModelName: "Canon iR-ADV 4525/4535 PS"
#-----------------------------------------------------------------------

*RepeatJob copies
 
*StaplePos org.savapage-finishings-staple
*StaplePos *None 3
 
*Punch org.savapage-finishings-punch
*Punch *None  3
 
# ............................................................... 
# Let MFP decide on output-bin. 
# ............................................................... 
*CNOutput output-bin
*CNOutput *Auto   auto 
 
*Booklet org.savapage-finishings-booklet
*Booklet Left   toleft-totop
 
#----------------------------------------------------------------------
# Combination of sheet-collate and org.savapage-finishings-jog-offset 
# determines the Finishing PPD option.
#
# Hack: use fixed OptimiseRepeatJob=True to "neutralize" sheet-collate
# PPD option, and use SPConstraint to make uncollate impossible.
#----------------------------------------------------------------------
*OptimiseRepeatJob sheet-collate
*OptimiseRepeatJob *True  collated
 
# jog-offset 'none' is the only option
*Finishing org.savapage-finishings-jog-offset
*Finishing  Col  3
 
*SPExtra/org.savapage-finishings-booklet/toleft-totop: booklet-toleft-totop-a3 \
   media/iso_a3_297x420mm \
   *media/iso_a4_210x297mm \
   *number-up/1 \
   *sides/one-sided \
   *BindMode/SaddleStitch \
   *Booklet/Left
 
*SPExtra/org.savapage-finishings-booklet/toleft-totop: booklet-toleft-totop-a4 \
   media/iso_a4_210x297mm \
   *media/iso_a5_148x210mm \
   *number-up/1 \
   *sides/one-sided \
   *BindMode/SaddleStitch \
   *Booklet/Left
 
*SPConstraint: booklet-uncollated \
    org.savapage-finishings-booklet/!none \
    sheet-collate/uncollated
    
# end-of-file