# -----------------------------------------------------------------------------
# This file is part of the SavaPage project <https://www.savapage.org>.
# Copyright (c) 2011-2020 Datraverse B.V.
# Author: Rijk Ravestein.
#
# SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file; if not, see <https://www.gnu.org/licenses/>
#
# For more information, please contact Datraverse B.V. at this
# address: info@datraverse.com
# -----------------------------------------------------------------------------

#-----------------------------------------------------------------------
#*PCFileName: "CEIRADV8.PPD"
#*ModelName: "Canon iR-ADV 8505 PS"
#-----------------------------------------------------------------------

*RepeatJob copies
 
*RepeatJob copies
 
*Staple org.savapage-finishings-staple
*Staple 1PLU  20
*Staple 1PLB  21
*Staple 1PRU  22
*Staple 2PL   28
 
*Punch org.savapage-finishings-punch
*Punch *None  3
*Punch 2PL   74
*Punch 2PU   75
*Punch 2PB   77
*Punch 4PL   82

# ............................................................... 
# NOTE: TrayB is omitted, because it is reserved for jog-offset
# and therefore  is not applicable for stapled output.
# ............................................................... 
*CNOutput output-bin
*CNOutput *Auto   auto 
*CNOutput TrayA   stacker-1
*CNOutput TrayC   stacker-2

*Booklet org.savapage-finishings-booklet
*Booklet *Default  none
 
#----------------------------------------------------------------------
# Combination of sheet-collate and org.savapage-finishings-jog-offset 
# determines the Finishing PPD option.
#
# Hack: use fixed OptimiseRepeatJob=True to "neutralize" sheet-collate
# PPD option, and use SPExtra rules to set Finishing PPD option for 
# org.savapage-finishings-jog-offset
#----------------------------------------------------------------------
*OptimiseRepeatJob sheet-collate
*OptimiseRepeatJob *True  collated
*OptimiseRepeatJob  True  uncollated
 
*Finishing org.savapage-finishings-jog-offset
*Finishing  dummy      3
*Finishing *dummy     14
 
*SPExtra/sheet-collate/collated: collated-job-offset-on \
    org.savapage-finishings-jog-offset/!3 \
    *Finishing/OffCol
 
*SPExtra/sheet-collate/collated: collated-job-offset-off \
    org.savapage-finishings-jog-offset/3 \
    *Finishing/Col
 
*SPExtra/sheet-collate/uncollated: uncollated-job-offset-on \
    org.savapage-finishings-jog-offset/!3 \
    *Finishing/OffGr
 
*SPExtra/sheet-collate/uncollated: uncollated-job-offset-off \
    org.savapage-finishings-jog-offset/3 \
    *Finishing/None
 
# end-of-file
