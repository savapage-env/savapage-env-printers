<!-- 
    SPDX-FileCopyrightText: 2011-2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: GPL-3.0-or-later 
-->

## Canon CQue Driver 4.0.1

CQue Driver for Canon iR, CLC, LPB and MF laser devices is free to download from the [Canon support site](https://www.canon-europe.com/support/business-product-support/). 

### CUPS

For **stapling** (stitch) of more than 25 copies to work correctly, apply the following options in CUPS Web Interface for all queues involved:

  * `Repeat Job (0-999): Custom`
    * If this option is not applied, the number of stapled copies will be one (1) for any `-o RepeatJob=26` or higher. 
  * `RepeatJob: 1` 
    * Any value > 1 will do, but 1 is a safe value.
  * `Optimise Repeat Job: ON`
    * This will only make one (1) log entry in the MFP for a single job with more than one (1) stapled copy. 

### SavaPage

According to [Issue #987](https://issues.savapage.org/view.php?id=987) set configuration key:

`ipp.job-name.space-to-underscore.enable : Y`

Unfortunately the CQue PPD’s are inconsistent and cumbersome at some major points:

  * Color MFP’s (iR-ADV C xxxx) need the options `StaplePos` and `StapleType` to get stapled output, while black-and-white MFP’s expect a single option `Staple`.
  * Collate and jog-offset options are combined into a single `Finishing` option.

Sample .ppde files for [Canon iR-ADV 4525/4535 PS](./cel-iradv4525-35-ps), [Canon iR-ADV 8505 PS](./cel-iradv8505-ps) and [Canon iR-ADV C7565 PS](./cel-iradvc7565-ps) are present.

---
This document is part of the SavaPage project <https://www.savapage.org>, copyright (c) 2011-2020 Datraverse B.V. and licensed under the [GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl.txt) version 3, or (at your option) any later version. Join the [SavaPage Community](https://wiki.savapage.org) and help build Open Printing Solutions.

[<img src="../../../../img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)
