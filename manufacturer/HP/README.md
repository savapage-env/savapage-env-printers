<!-- 
    SPDX-FileCopyrightText: 2011-2023 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: GPL-3.0-or-later 
-->

## HP

### SavaPage

Sample .ppde files:

  * [HP Officejet Pro 9010 Series](./hp-officejet_pro_9010_series.ppde)
  * [HP Envy 7640 Series](./hp-envy_7640_series.ppde)

---
This document is part of the SavaPage project <https://www.savapage.org>, copyright (c) 2011-2023 Datraverse B.V. and licensed under the [GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl.txt) version 3, or (at your option) any later version. Join the [SavaPage Community](https://wiki.savapage.org) and help build Open Printing Solutions.

[<img src="../../img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)
