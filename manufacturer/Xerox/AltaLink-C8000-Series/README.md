<!-- 
    SPDX-FileCopyrightText: 2011-2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: GPL-3.0-or-later 
-->

## Xerox AltaLink C8000 Series

AltaLink C8030/C8035/C8045/C8055/C8070 is a color multi-functional printer.

### CUPS

[Xerox-AltaLink-C8000-Series-SavaPage.ppd](./Xerox-AltaLink-C8000-Series-SavaPage.ppd)

### SavaPage

[Xerox-AltaLink-C8000-Series-SavaPage.ppde](./Xerox-AltaLink-C8000-Series-SavaPage.ppde)

### Status

_All printer functions are supported, finishings included._

---
This document is part of the SavaPage project <https://www.savapage.org>, copyright (c) 2011-2020 Datraverse B.V. and licensed under the [GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl.txt) version 3, or (at your option) any later version. Join the [SavaPage Community](https://wiki.savapage.org) and help build Open Printing Solutions.

[<img src="../../../img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)
