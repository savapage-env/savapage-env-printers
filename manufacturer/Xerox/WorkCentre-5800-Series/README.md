<!-- 
    SPDX-FileCopyrightText: 2011-2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: GPL-3.0-or-later 
-->

## Xerox WorkCentre 5800 Series

WorkCentre 5865/5875/5890 is a black-and-white multi-functional printer. The 5800 Series is no longer sold as new, and has been succeeded by the WorkCentre 5800i Series.

### CUPS

[Xerox-WorkCentre-5800-Series-SavaPage.ppd](./Xerox-WorkCentre-5800-Series-SavaPage.ppd)

### SavaPage

[Xerox-WorkCentre-5800-Series-SavaPage.ppde](./Xerox-WorkCentre-5800-Series-SavaPage.ppde)

### Status

_All printer functions are supported, finishings included._

### Notes

PPD of [AltaLink-C8000-Series](../AltaLink-C8000-Series/README.md) is backwards compatible with the WorkCentre 7800 Series, so internals of .ppde and .ppde files are identical (except for the XROutputColor option).

---
This document is part of the SavaPage project <https://www.savapage.org>, copyright (c) 2011-2020 Datraverse B.V. and licensed under the [GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl.txt) version 3, or (at your option) any later version. Join the [SavaPage Community](https://wiki.savapage.org) and help build Open Printing Solutions.

[<img src="../../../img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)
