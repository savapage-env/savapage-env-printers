<!-- 
    SPDX-FileCopyrightText: 2011-2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: GPL-3.0-or-later 
-->

## Xerox D95/D110/D125 Copier/Printer

The D95/D110/D125 is a black-and-white copier/printer.

### CUPS

[Xerox-D95-D110-D125-Copier-Printer-SavaPage.ppd](./Xerox-D95-D110-D125-Copier-Printer-SavaPage.ppd)

### SavaPage

[Xerox-D95-D110-D125-Copier-Printer-SavaPage.ppde](./Xerox-D95-D110-D125-Copier-Printer-SavaPage.ppde)

### Status

_All printer functions are supported, finishings included._

---
This document is part of the SavaPage project <https://www.savapage.org>, copyright (c) 2011-2020 Datraverse B.V. and licensed under the [GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl.txt) version 3, or (at your option) any later version. Join the [SavaPage Community](https://wiki.savapage.org) and help build Open Printing Solutions.

[<img src="../../../img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)
