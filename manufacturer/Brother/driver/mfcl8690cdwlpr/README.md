<!-- 
    SPDX-FileCopyrightText: 2011-2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: GPL-3.0-or-later 
-->

## Brother MFC-L8690CDW LPR Driver 1.3.0

Brother MFC-L8690CDW LPR printer driver is free to download from the [Brother support site](https://support.brother.com/g/b/downloadlist.aspx?c=gb&lang=en&prod=mfcl8690cdw_eu_as&os=128). 

### SavaPage

A sample [brother-mfcl8690cdwlpr-1.3.0.ppde](./brother-mfcl8690cdwlpr-1.3.0.ppde) file is present.

---
This document is part of the SavaPage project <https://www.savapage.org>, copyright (c) 2011-2020 Datraverse B.V. and licensed under the [GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl.txt) version 3, or (at your option) any later version. Join the [SavaPage Community](https://wiki.savapage.org) and help build Open Printing Solutions.

[<img src="../../../../img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)
